
# --- review ---

# arrays: 
- data structure that lets you store and organize multiple pieces of data in any order.
    - visual representation is:
    - ["a", "b", "c", "d", "e"]
- FIXED SIZE that's specified at initialization
- operations include:
    - set a value at an index:  `arr[i] = 3;`
    - get a value at an index: `System.out.println(arr[0]);`
    - get the fixed size of the array: `arr.length`
- built into the language with special [] syntax  


# ArrayList - dynamic (able to change) arrays
- data structure that lets you store and organize multiple pieces of data in any order.
    - visual representation is:
    - ["a", "b", "c", "d", "e"]
- FLEXIBLE SIZE, all operations can adjust size accordingly
- operations include:
    - set a value at an index: `list.set(i, 3);`
    - get a value at an index: `System.out.println(list.get(0));`
    - get the flexible size of the list: `list.size()`
    - inserting a value at an index and the later values get pushed down the list to the right: 
        - `list.add(0, 3);` adds the value 3 at index 0. All the existing values get shifted to the right by one.
        - [2, 4, 6]  --> `list.add(0,3)` ---> [3, 2, 4, 6]
    - removing a value at an index and the later values get pushed to the left:
        - list.remove(0); removes the value at index 0. All the existing values get shifted to the left by one.
        -  [3, 2, 4, 6] --> `list.remove(0)` ---> [2, 4, 6]
    - and many more.
- implemented as a Java class.  It's a blueprint class like we're used to in a file called ArrayList.java.  We could even write the code ourselves for this and it wouldn't be too crazy.  

So let's do it!

# --- new content ---

# ArrayIntList

ArrayIntList - let's do an activity where we reimplement ArrayList just for integers

why are we reimplementing ArrayList:
- more Java practice as an exercise
- helps us understand ArrayList's actual implementation as an educational exercise 
- we're going to use it later for the Checkpoint
- you'd be surprised how often companies want to reimplement existing things but with their own custom flair.  Lots of companies make their own internal versions of tools so they can change small things. Amazon has their own versions of org.apache.commons.lang3.StringUtils and Android build systems, Facebook has their own version of git/mercurial, etc.

Let's make a class called ArrayIntList and implement a few methods: 

- public void add(int value) - adds given value to the end of the list
- public int get(int index) - gets the value at the given index
- public String toString() - returns a comma separated String representation of the list surrounded by square brackets
- public int add(int index, int value) adds given value at the specific index 

# What fields do we need?

## elementData
Let's use a big/spacious array to store the int elements, call it `elementData`.  We'll initialize it to be bigger than we need so that we can freely add more data without running out of space.

For the external representation, an ArrayList (or our custom ArrayIntList) could look like:

[5, -3, 2, 10]

And on the inside of the class, the private int[] field `elementData` would actually look like:

[5, -3, 2, 10, 0, 0, 0, 0 ,0 ,0 ...]

We'll maintain the illusion that the size of our ArrayIntList is flexible by just having a super big array and then doing some extra steps to make sure we only show the client the real data and not any of the uninitialized /garbage data. 

## size
To do this we'll also have to keep track of the actual size of the list, called `size`. This will help us know where the end of the list is. When we want to add new elements to the back of the list or want to loop through the valid data, this will help us know where the valid data ends.

```java
public class ArrayIntList {
    private int[] elementData;
    private int size;

    public ArrayIntList() {
        this.elementData = new int[100];
        this.size = 0;
    }
}
```

Above would be our current code. Using these two fields, let's try to write the `add(int value)` and `toString()` methods together.

# implementing ArrayIntList's methods

## add(int value)
Since we're adding to the back of the list, we need to figure out what index in the array represents the back. It turns out `size` is what we need.  When `size` is 0, we actually want to set index 0 to the newly added value.  When `size` is 1 (index 0 is occupied), we actually want to add at index 1.  So goes the pattern and we can always just set `elementData[size] = value`. And remember to update size for the next time `add` is called.

```java
public void add(int value) {
    elementData[size] = value;
    size++;
}
```


## toString()

This should return a comma separated String representation of the list, surrounded by square brackets. So [3, 4, 6] for example.  Make sure to only look at the valid data and not any of the garbage uninitialized data.

Try to implement `toString()` on your own before reading our implementation.  


```java
public String toString() {
    if (size == 0) {
        return "[]"
    } else {
        String result = "[" + elementData[0];
        for (int i = 1; i < size; i++) {
            result += ", " + elementData[i];
        }
        return result + "]";
    }
}
```

We create a String result to build up the answer, and initialize it to be the left bracket and the first element outside the loop so each element after the first has a comma before it.

We also have a separate if branch for if the size is 0 because then we don't want to use the 0th element in our result.

## get(int index)

This one is not as much code as `toString`. Just return the value at the index -- try it on your own before looking at the solution below.

```java
public int get(int index) {
    return elementData[index];
}
```

We could also do some error handling for if an invalid index is passed (index < 0 or index >= size) but don't worry about that for now.

## add(int index, int value)

This one is the most difficult out of the methods we've implemented.  If we want to implement the functionality of inserting a value at a given index, we need to remember that you can only set values to change the contents of the arrays.  A naive implementation of this method could be the following code:

```java

public void naiveAdd(int index, int value) {
    elementData[index] = value;
}
```

But this is no good because we overwrote the existing value at that index.  If we actually want to squeeze that new value in there, we have to manually move over all those values that come later in the list to the right by one. Here's a closer-to-correct yet still naive implementation.

```java
public void naiveAdd2(int index, int value) {
    for (int i = index; i < size - 1; i++) {
        elementData[i + 1] = elementData[i];
    }
    elementData[index] = value;
    size++;
}
```

Take a minute to try to reason what's wrong with this one before reading onwards.

This implementation is close to correct, as it remembers to shift over values as they should all come 1 index later now. But the order that it does this shifting means we're actually copying over the same value each time.

For example for an elementData of [2, 4, 6, 8, -, - ,- ...] and size of 4.  If you called `naiveAdd(0, 10)`, trace through what would happen.  
1. We would run `elementData[i + 1] = elementData[i]` for i = 0. 
    1. now the array looks like [2, 2, 6, 8]
2. We would run `elementData[i + 1] = elementData[i]` for i = 1.
    1. now the array looks like [2, 2, 2, 8]
and so on.  If we want to do this shifting correctly in this particular case, we can loop backwards to fix our issues. A correct implementation of add at an index could be the following code:

```java
public void add(int index, int value) {
    for (int size; i > index; i--) {
        elementData[i] = elementData[i - 1];
    }
    elementData[index] = value;
    size++;
}
```